# vim: noexpandtab tabstop=3 shiftwidth=3 :
CC			= gcc
CFLAGS	= -Isubhook -D_GNU_SOURCE -fPIC
STRIP	  	= sstrip

.DEFAULT_GOAL := bin/moth.so

bin/:
	mkdir bin

bin/moth.so: src/moth.c bin/
	$(CC) $(CFLAGS) src/moth.c -ldl -shared -o bin/moth.so

release: CFLAGS += -DMOTH_RELEASE -g0
release: bin/moth.so
	$(STRIP) bin/moth.so

clean:
	-rm bin/moth.so
