#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#define SUBHOOK_STATIC
#include <subhook.c>

#if defined(MOTH_RELEASE)
#   define moth_log(...) (0)
#   define moth_backtrace()
#else

#include <execinfo.h>
#include <stdarg.h>

static int
moth_log (char const* fmt, ...)
{
   va_list va;
   int size;

   fprintf(stderr, "Moth: ");
   va_start(va, fmt);
   size = vfprintf(stderr, fmt, va);
   va_end(va);

   return size;
}

#define moth_backtrace() \
   { \
      void*  frames[100]; \
      char** frameSyms; \
      int    frameCount; \
      \
      frameCount = backtrace(frames, 100); \
      frameSyms  = backtrace_symbols(frames, frameCount); \
      moth_log("%s call depth is %i frames:\n", __func__, frameCount); \
      \
      if (frameSyms) \
      { \
         for (int idx = 0; idx < frameCount; ++idx) \
         { \
            moth_log("%-3i : %s\n", idx, frameSyms[idx]); \
         } \
         free(frameSyms); \
      } \
      else \
      { \
         moth_log("\tError: could not generate symbolic trace\n"); \
      } \
   }
#endif


asm (
   "rep_GetLicenseStatus:\n"
      // Original is 0x88, but we don't care about the stack contents for GetLicenseStatus
      "sub $0xA8, %rsp\n"
      "mov $1, %eax\n"
      "add $0xA8, %rsp\n"
      "ret\n"
);
extern void rep_GetLicenseStatus(); // 0x4FA420 @ 4.2.10; Return in EAX
static subhook_t sh_GetLicenseStatus;

asm (
   "rep_GetLicenseOrd:\n"
      "mov $1, %eax\n"
      "ret\n"
);
extern void rep_GetLicenseOrd(); // 0x4588A0 @ 4.2.10; Return in EAX
static subhook_t sh_GetLicenseOrd;

static void __attribute__ ((constructor))
moth_init (void)
{
   unsetenv("LD_PRELOAD"); // Prevent moth from breaking children

   // Info for 4.2.10
   // _start          = 0x4576B0 .. 0x4576DA
   // ... 0xA2D70 ...
   // GetLicenseState = 0x4FA420 .. 0x4FA64D
   // main            = 0x560BC0 .. 0x5611DC
   sh_GetLicenseStatus  = subhook_new((void*) 0x4FA420, (void*) rep_GetLicenseStatus, SUBHOOK_OPTION_64BIT_OFFSET);
   sh_GetLicenseOrd     = subhook_new((void*) 0x4588A0, (void*) rep_GetLicenseOrd, SUBHOOK_OPTION_64BIT_OFFSET);

   subhook_install(sh_GetLicenseStatus);
   subhook_install(sh_GetLicenseOrd);
}
